
### Antonin Cosse BTS SIO OPTION SLAM

# AP Score resto AP1/

### Question 1.1 : A l'aide de la documentation officielle PHP, rappeler le rôle des mots clés suivants  :   include_once,  include

La fonction "Include"  permet d’importer d'autre fichier PHP dans le fichier présent. 

### Question 1.2 :  Déterminer la valeur de la variable $fichier en affichant son contenu à la ligne précédant la balise de fin  de PHP dans  index.php.

La variable $fichier contient le fichier "ListeResto.php"

### Question 1.3 :  En partant du fichier  index.php, schématiser l'ensemble des fichiers utilisés  (include ou include_once)  pour afficher la liste des restaurants.

Le fichier Index.php inclue le fichier GetRacine.php a la ligne 2. et le fichier ControleurPrincipale.php a la ligne 3. 

### 1.4.  Relever  l'ensemble  des  requêtes  SQL  contenues  dans  les  fonctions  déclarées  dans  le  fichier  bd.resto.inc.php.

| L | Fonction | Requête |
|--|--|--|
| 5 |  GetRestoByIDR() | select * from resto where idR=idR |
| 21 | getResto() | select * from resto | 
| 41 | getRestosByNomR() | select * from where nomR like :NomR |
| 63 | getRestoByadresse() | select from resto where voieAdrR like :voieAdrR and cpR and villeR like :villeR |

### Question 1.5 :  Quels sont les points communs de ces requêtes SQL ?

Ces requêtes a pour pojnt commun la table resto, mais aussi le "select" qui selection la table resto

### Question 1.6:  En observant le nom des fonctions, le résultat affiché, et la requête contenue dans la fonction, expliquer  d'une manière très générale ce que fait chacune des fonctions présente dans le script  bd.resto.inc.php.

| Focntion | Description |
|--|--|
|  GetRestoByIDR() | Permet d'afficher toutes les tables |
| getResto() | Permet d'afficher une table avec un ID | 
| getRestosByNomR() | Permet d'afficher une table avec un nom |
|getRestoByadresse()| Permet d'afficher une table avec une rue, un code postal et une ville |

### Question 1.7:  Quelle fonction définie dans le script  bd.resto.inc.php  est utilisée dans  listeRestos.php  ?

Le fichier listeRetos.php utilise toute les fonctions du fichier bd.resto.inc.php

### Question 2.1:  Trouve-t-on des éléments de CSS ou de HTML dans les fichiers des dossiers controleur et modele  ?

Non il n'y a pas de trace de HTML/CSS pure dans les fichiers des dossiers "COntroleur" et "Modèle"

### Question 2.2:  Dans quel dossier sont contenus les fichiers gérant les éléments de HTML et de CSS  ?

Les fichier CSS sont contenus dans le dossier "CSS/" et le html dans le dossier"vue/"

### Question2.3 :  Consulter le code source de la page lors d'une visite de l'URL "http:·... /index.php?action=liste".  Retrouve-t-on le même contenu que celui des scripts du dossier vue ?

Le site diffère au niveau des variable il prend les données présentes sur la bases de donnée

### Question 2.4.  Le  code  PHP  contenu  dans  vueListeRestos.php  est-il  toujours  visible  après  réception  par  le  navigateur ? Par quoi est il remplacé ?

Non le code php pure n'est pas visible pas le navigateur mais a ete remplacer par le nom des Restaurant et leur ville grace au requête mySQL.

### Question 2.5:  Trouve-t-on dans d’autres fichiers que ceux du dossier  modele  des références à la base de données  ?

Non chaque dossier correspond a une choses, "Modèle" correspond a toute les requête mySQL et sont appeler par les autre fichier si besoin.

### Synthèse : expliquer globalement le rôle des scripts contenus dans les dossiers suivants

#### /Modèle/
                     -Le dossier contient toute les références à la bases de donnée
#### / Vue/
					 -Le dossier contient toute les références HTML du site Web
#### /CSS/
				     -Le dossier contient toute les références CSS du site web

### Question 3.1:  Quelle fonction d'accès aux données est utilisée dans ce contrôleur ? Que permet-elle de récupérer dans  la base de données ?

La fonction include_once récupère les données  GET,POST et SESSION

### Question 3.2 : 3.2.  Les données récupérées sont-elles affichées à l'écran ? L'affichage est-il fait dans le script contrôleur ?

Oui les données trouvées dans la bases de donnée sont affichée a l'écrans via le script "VueListeRestos.php"

### Question 3.3:  Quels scripts sont inclus dans les dernières lignes du contrôleur ? Quels sont leurs rôles ?
| Script | Description |
|--|--|
|  entete.html.php | Permet de générer l'entête du site web |
| VueListeRestos.php | Permet d'afficher via mySQL la liste de tout les restaurants présents sur la bases de donnée| 
| pied.html.php | Permet de générer le pied de page HTML  |

### Question 3.4: Quelle donnée est transmise au contrôleur en méthode GET ?

 GET transmet les données via la bases de donnée avec la variable getRestoByIDR car sont  argument est "idR".

### Question 3.5.  Rappeler  le  rôle  de  la  fonction  getRestoByIdR().  Pourquoi  cette  méthode  a-t-elle  besoin  d'un  paramètre ?

Le Rôle de getRestosByidR a pour but de prendre les information du tables avec l’identifiant idR
, Cette méthode a besoins d'un paramètre car chaque table a un identifiant cela permet de bien lire/modifier la bonne tables avec cette identifiant unique

### Question 3.6:  Explorer les fichiers "vue" inclus à la fin du fichier contrôleur et repérer les lignes où sont utilisées cette  variable.

Cette variable est utlisée dans les lignes :

                                                            `vueDetailResto.php`
 - L.2
 - L.9
 - L.15
 - L.16
 - L.17
 - L.18
 - L.30

 ### Question 3.7.  Deux autres variables créées dans le contrôleur sont affichées dans la vue. Lesquelles ?

Les variables  $MenuBurger$ et $Unresto$ sont présente dans le dossier vue.

### Question 3.8:  Le contrôleur gère-t-il directement l’accès aux données  ?  

Non, l'accès aux données est géré dans les fonctions que l'on trouve dans le modèle.



# AP Score resto AP2/

### Question 1.1.  La fonction isLoggedOn()  retourne un booléen, soit  true, soit  false  selon que l'utilisateur est connecté ou  pas sur le site. Lors du 1er appel à la fonction  isLoggedOn()  l'utilisateur est il connecté ?

Non l'or du première appel de la foncions il n'est pas connectée.
### Question 1.2.  Lors du 2ème appel à la fonction  isLoggedOn()  l'utilisateur est il connecté ?
Oui l'or du deuxième appel il est connectée au site web.

### Question 1.3.  Quelle fonction permet la connexion de l'utilisateur ? Quelle est sa définition  (prototype, signature)  ?

La fonction qui permet a l’utilisateur de ce connecter est login(), c'est paramètre sont le $mailU (le mail de connexion) et $mdpU ( le mot de passe de connexion).

### Question 1.4.  À l'aide des questions précédentes et en observant la section de test et le résultat d'exécution du script  authentification.inc.php, indiquer le rôle des fonctions suivantes :

| Focntion | Description |
|--|--|
|login() | Cette fonction permet de se connecter en tant qu’utilisateur.|
| isLoggedOn()|Permet de vérifier si l'on est déjà connecter au site web.| 
| getMailULogged()| Prend la mail de connexion.|
| logout()| Permet d ece déconnecter du site web. |

### Question 1.5.  À l'aide des annexes 2 et 3 compléter le schéma ci-dessous en indiquant :
 
 ```mermaid
graph 
A[ Ordinateur] -- Method = POST ---> B((Controleur))
A -- Name = mailU  -->B
A -- Name =  mdpU -->B
B --  logged ---> C[vue]
B -- not logged   ---> D[vue]
```
## Partie AP3 -

### Question 1.1. Quelles fonctions définies dans le modèle sont utilisées dans le contrôleur detailResto.php. 

| Focntion | Description |
|--|--|
| GetRestoByIDR() | Permet de prendre une ID d'un restaurant dans la basse de donnée |
| getTypesCuisineByIdr() | Permet de prendre une id dans la table cuisne dans la basse de donnée   |
| getPhotobyIDR| Permet de prendre une photo dans la base de données |

### Question 1.2. Quelles annexes présentent l'affichage du résultat d'exécution des fonctions du modèle trouvées à la question précédente ?

Le script VuedetailRestop.php permet d'afficher le résultat des fonction précédente .

### 1.3. Comment est composée chacune des 3 cellules du tableau $lesPhotos ?
Les cellules sont composée de "idP" , "CheminP" et "idR".

### 1.4. Repérer dans l'annexe 1 l'instruction PHP permettant d'afficher le nom du fichier de la première photo.
 L'instruction : 
             <img src="photos/<?= $lesPhotos[0]["cheminP"] ?>" alt="photo du restaurant" />

### 1.5. Quel est le rôle de l'instruction if(count($lesPhotos) > 0) ? Quel est son rôle dans le code de la vue ? 
Permet d'afficher les photos a la suite et non la même photos en boucle

### 1.6. Quelle est la syntaxe générale permettant d'accéder à un champ (idP, CheminP ou idR) contenu dans la variable $lesPhotos ?
la syntaxe général pour accéder au champ(idP, CheminP ou ID) est $lesPhotos[0]  (cheminP) permet daccéder par exemple a la cellule 0 et au "CheminP"

### 2.1. Quel contrôleur produit la variable $lesTypesCuisine ? Préciser le nom de la fonction et du modèle. 
le controleur "getTypesCuisineByIdR($idR)" inclu dans la fonction $lesTypesCuisine

### 2.2. Schématiser le contenu de la variable $lesTypesCuisine 
la variable contient un idR et typeCuisine 
 
### 2.3. Combien de types de cuisine sont contenus dans cette variable ? 
La variable peut contien le idR de la cuisine mais aussi le TypeCuisine de celui ci  

### 2.4. Quelle est la syntaxe permettant d'accéder au libelle d'un type de cuisine contenu dans la variable ?
la syntaxe est la suivantes : $cnx->prepare("select typeCuisine.* from typeCuisine,proposer where typeCuisine.idTC = proposer.idTC and proposer.idR = :idR");

### 3.1. Quel contrôleur produit la variable $unResto ? Préciser le nom de la fonction et du modèle.
le contrôleur "getRestoByIdr($idR)"
### 3.2. Schématiser le contenu de la variable $unResto 
le contenue de la variable a l'idR d'un restaurant son adresse ('numAdrR') sa voie ('voieAdr') le code postal ('cpR') et la ville ('villeR')
### 3.3. Quelles sections de code dans la vue utilisent cette variable ? 
dans vueDetailResto.php cette variable est utilisée ici :
l.1 : <?= $unResto['nomR']; ?>
l.27: <?= $unResto['numAdrR']; ?>
l.28    <?= $unResto['voieAdrR']; ?>  
L.29   <?= $unResto['cpR']; ?>
L.30    <?= $unResto['villeR']; ?>

### 3.4. Combien de restaurants sont contenus dans cette variable ? 

tout les restaurants présent sur le site sont dans cette variable

### 3.5. Quelle est la syntaxe permettant d'accéder au nom d'un restaurant contenu dans la variable ?

la fonction $unResto['nomR']; permet d'afficher le restaurant par sont nom 
